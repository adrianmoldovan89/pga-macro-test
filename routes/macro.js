module.exports = function (app, addon) {

  // Returns a HTTP client which can make calls to our host product.
  // @param clientKey formed when app created.
  // @param userKey formed when app created.
  // @returns {*} http client
  function getHTTPClient(clientKey, userKey) {
    return addon.httpClient({
      clientKey: clientKey,
      userKey: userKey,
      appKey: addon.key
    });
  }

};
