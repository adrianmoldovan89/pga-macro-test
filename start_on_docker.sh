#!/usr/bin/env bash
docker run --detach \
    --name epdft \
    --env "VIRTUAL_HOST=epdft.pgaconsult.ro" \
    --env "VIRTUAL_PORT=3000" \
    --env "LETSENCRYPT_HOST=epdft.pgaconsult.ro" \
    --env "LETSENCRYPT_EMAIL=alex.moldovan@pgaconsult.ro" \
    --env "NODE_ENV=production" \
    epdft:0.0.1
